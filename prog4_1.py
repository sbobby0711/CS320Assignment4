def validNumber(str):
    if len(str) == 0:
        return False
    if str[0] != '-' and str[0] != '+' and (str[0] < '0' or str[0] > '9'):
        return False
    for i in str[1:]:
        if i < '0' or i > '9':
            return False
    return True

validToken  = {"pop": 1,
               "add": 1, 
               "sub": 1, 
               "mul": 1, 
               "div": 1, 
               "mod": 1,
               "skip": 1,
               "push": 2,
               "save": 2,
               "get": 2}

def Tokenize(str):
    tokens = str.split()
    for token in tokens:
        if not validNumber(token) and not (token in validToken):
            raise ValueError("Unexpected token: " + token)
    return tokens

def Parse(tokens):
    if not (tokens[0] in validToken) or len(tokens) != validToken[tokens[0]]:
        raise ValueError("Parse error: " + " ".join(tokens))
    return tokens