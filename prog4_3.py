import sys
sys.path.append(".")
from prog4_1 import * 
from prog4_2 import * 
print("Assignment #4-3, Weihao Chang, wchang930711@gmail.com")

lines = []
file  = open(sys.argv[1], "r")
for line in file:
    lines.append((line, Parse(Tokenize(line))))

  
stack = StackMachine()
n = len(lines)
while stack.CurrentLine >= 0 and stack.CurrentLine < n:
    try:
        ln = lines[stack.CurrentLine][1]
        ret = stack.Execute(ln)
        if not (ret is None):
            print(ret)
    except (IndexError) as arg:
        print("Line " + str(stack.CurrentLine) + ": `" + " ".join(ln) + "' caused " + arg.args[0] + ".")
        sys.exit()

if stack.CurrentLine >= n:
    print("Program terminated correctly")
else:
    print("Trying to execute invalid line: " + str(stack.CurrentLine))