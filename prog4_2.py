funcs = {
    "add": lambda a,b: a + b,
    "sub": lambda a,b: a - b,
    "mul": lambda a,b: a * b,
    "div": lambda a,b: a / b,
    "mod": lambda a,b: a % b
}
class StackMachine:
    def __init__(self):
        self.mem = {}
        self.stack = []
        self.CurrentLine = 0

    def Execute(self, token):
        if token[0] == "push": 
            self.stack.append(int(token[1]))
            self.CurrentLine += 1
        if token[0] == "pop":
            if len(self.stack) > 0:
                ret = self.stack.pop()
                self.CurrentLine += 1
                return ret
            else:
                raise IndexError("Invalid Memory Access")
        if token[0] == "add" or token[0] == "sub" or token[0] == "mul" or token[0] == "div" or token[0] == "mod":
            if len(self.stack) > 1:
                lhs = self.stack.pop()
                rhs = self.stack.pop()
                self.stack.append(funcs[token[0]](lhs, rhs))
                self.CurrentLine += 1
            else:
                raise IndexError("Invalid Memory Access")
        if token[0] == "skip":
            if len(self.stack) > 1:
                lhs = self.stack.pop()
                rhs = self.stack.pop()
                if lhs == 0:
                    self.CurrentLine += rhs
                self.CurrentLine += 1
            else:
                raise IndexError("Invalid Memory Access")
        if token[0] == "save":
            if len(self.stack) > 0:
                op = self.stack.pop()
                self.mem[int(token[1])] = op
                self.CurrentLine += 1
            else:
                raise IndexError("Invalid Memory Access")
        if token[0] == "get":
            if not (int(token[1]) in self.mem):
                raise IndexError("Invalid Memory Access")
            self.stack.append(self.mem[int(token[1])])
            self.CurrentLine += 1